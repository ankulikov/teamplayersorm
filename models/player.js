"use strict";

module.exports = function (sequelize, DataTypes) {
    var Player = sequelize.define("Player", {
        name: DataTypes.STRING,
        isGoalkeeper: DataTypes.BOOLEAN
    });

    return Player;
};


