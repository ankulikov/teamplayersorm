var express = require('express');
var router = express.Router();
var models = require('../models');

router.post('/create', function (req, res, next) {
    models.Team.create({
        name: req.param('name')
    }).then(function () {
        res.redirect('/');
    });
});

router.get('/:team_id/destroy', function (req, res, next) {
    models.Team.find({
        where: {id: req.params.team_id}
    }).then(function (team) {
        team.destroy().then(function () {
            res.redirect('/');
        });
    });
});


router.post('/:team_id/players/create', function (req, res, next) {
    models.Team.find({
        where: {id: req.params.team_id}
    }).then(function (team) {
        models.Player.create({
            name: req.body.name,
            isGoalkeeper: req.body.isGoalkeeper == 'on'
        }).then(function (player) {
            team.addPlayer(player).then(function () {
                res.redirect('/');
            });
        })
    });
});

router.get('/:team_id/players/:player_id/destroy', function (req, res, next) {
    models.Team.find({
        where: {id: req.params.team_id}
    }).then(function (team) {
        models.Player.find({
            where: {id: req.params.player_id}
        }).then(function (player) {
            player.destroy().then(function () {
                res.redirect('/');
            });
        })
    });
});

module.exports = router;


