var express = require('express');
var router = express.Router();
var models = require('../models');

/* GET home page. */
router.get('/', function(req, res, next) {
  models.Team.findAll({
    include: [models.Player]
  }).then(function (teams) {
    res.render('index', {
      title: 'Express',
      teams: teams
    })
  });
});

module.exports = router;
